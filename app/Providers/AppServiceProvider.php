<?php

namespace App\Providers;

use App\Crypto\Stellar\Server;
use Illuminate\Support\ServiceProvider;
use League\Fractal\Manager;
use League\Fractal\Serializer\DataArraySerializer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        # add Fractal to the container
        $this->app->singleton(Manager::class, function () {
            $fractal = new Manager();
            $fractal->setSerializer(new DataArraySerializer());
            $request = app('request');
            if ($request->query->has('include')) {
                $fractal->parseIncludes($request->query->get('include', ''));
            }
            return $fractal;
        });
        # crypto server
        $this->app->singleton(Server::class, function () {
            return new Server(config('services.stellar.horizon_server'));
        });
    }
}
