<?php

namespace App\Http\Controllers\Stellar;


use App\Crypto\Stellar\Currency\LumenMoneyFormatter;
use App\Crypto\Stellar\Models\Account;
use App\Crypto\Stellar\Server;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Accounts extends Controller
{

    public function create(Request $request, Server $server)
    {
        $server = new Server('https://horizon-testnet.stellar.org');
        try {
            $response = $server->friendbot()->createAccount($request->query('public_key', ''));
            return response()->json($response->getData());

        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }

    public function single(Request $request, Server $server, string $id)
    {
        try {
            $response = $server->accounts($id)->request('get');
            $model = $response->getData(false, Account::class);
            foreach ($model->balances as $balance) {
                var_dump([(new LumenMoneyFormatter())->format($balance->balance), $balance->asset_type]);
            }
            exit();
            return response()->json($model->toArray());

        } catch (\Throwable $e) {
            dd($e->getMessage());
        }
    }
}