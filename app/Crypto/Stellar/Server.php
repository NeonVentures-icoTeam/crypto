<?php

namespace App\Crypto\Stellar;


use App\Crypto\Stellar\Resources\Accounts;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Uri;

/**
 * Instantiation of the Stellar sdk starts with a server instance.
 *
 * @method \App\Crypto\Stellar\Resources\Accounts   accounts(string $id = null)
 * @method \App\Crypto\Stellar\Resources\Friendbot  friendbot(string $id = null)
 *
 * @package App\Crypto\Stellar
 */
class Server
{
    /** @var Uri */
    private $url;

    /**
     * Server constructor.
     *
     * @param string $url
     */
    public function __construct(string $url)
    {
        $this->url = new Uri($url);
    }

    /**
     * Returns the base URI for the server.
     *
     * @return Uri
     */
    public function getUrl(): Uri
    {
        return $this->url;
    }

    /**
     * Returns a HTTP client for the server.
     *
     * @return \GuzzleHttp\Client
     */
    public function getHttpClient(): Client
    {
        $options = [
            \GuzzleHttp\RequestOptions::ALLOW_REDIRECTS => true,
            \GuzzleHttp\RequestOptions::CONNECT_TIMEOUT => 30.0,
            \GuzzleHttp\RequestOptions::TIMEOUT => 30.0,
            \GuzzleHttp\RequestOptions::HEADERS => [
                'User-Agent' => 'kinetic-crypto-php/2018.1'
            ]
        ];
        # the client options
        if (!empty($this->url->getHost())) {
            $options['base_uri'] = $this->url->getScheme() . '://' . $this->url->getAuthority();
            $options['base_uri'] .= !empty($this->url->getPath()) ? '/' . $this->url->getPath() : '';
        }
        return new \GuzzleHttp\Client($options);
    }

    /**
     * Magic method for automatically creating the appropriate resource for calls on the server.
     *
     * @param string    $name
     * @param null      $arguments
     *
     * @return mixed
     */
    public function __call($name, $arguments = null)
    {
        $arguments = $arguments ?: [];
        # normalise the arguments list
        $resource = __NAMESPACE__ . '\\Resources\\' . title_case($name);
        # the resource full name
        if (!class_exists($resource)) {
            throw new \BadMethodCallException('The method '.$name.' does not exist.');
        }
        return new $resource($this, ...$arguments);
    }
}