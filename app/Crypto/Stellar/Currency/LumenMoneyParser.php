<?php

namespace App\Crypto\Stellar\Currency;


use Money\Currency;
use Money\Exception;
use Money\Exception\ParserException;
use Money\Money;
use Money\MoneyParser;

class LumenMoneyParser implements MoneyParser
{

    /**
     * Parses a string into a Money object (including currency).
     *
     * @param string               $money
     * @param Currency|string|null $forceCurrency
     *
     * @return Money
     *
     * @throws Exception\ParserException
     */
    public function parse($money, $forceCurrency = null)
    {
        if (is_string($money) === false) {
            throw new ParserException('Formatted raw money should be string, e.g. $1.00');
        }
        if (strpos($money, LumenCurrencies::SYMBOL) === false) {
            throw new ParserException('Value cannot be parsed as Lumens');
        }
        # XLM1.00
        $decimal = str_replace(LumenCurrencies::SYMBOL, '', $money);
        # 1.00
        $decimalSeparator = strpos($decimal, '.');
        # get the position of the decimal point
        $decimalFigures = substr($decimal, $decimalSeparator + 1);
        # get the string part after the decimal point
        $remainingDecimalPlaces = 7 - strlen($decimalFigures);
        # the remaining decimal places
        $extras = $remainingDecimalPlaces > 0 ? array_fill(0, $remainingDecimalPlaces, '0') : [];
        # extra decimal places
        $adjustedDecimal = substr($decimal, 0, $decimalSeparator) . '.' . $decimalFigures . implode('', $extras);
        # our new figure
        return new Money(str_replace('.', '', $adjustedDecimal), new Currency(LumenCurrencies::SYMBOL));
    }
}