<?php

namespace App\Crypto\Stellar\Currency;


use Money\Exception\FormatterException;
use Money\Money;
use Money\MoneyFormatter;
use Money\Number;

class LumenMoneyFormatter implements MoneyFormatter
{

    /**
     * Formats a Money object as string.
     *
     * @param Money $money
     *
     * @return string
     *
     * Exception\FormatterException
     */
    public function format(Money $money)
    {
        if (LumenCurrencies::SYMBOL !== $money->getCurrency()->getCode()) {
            throw new FormatterException('Lumen Formatter can only format Lumens');
        }

        $valueBase = $money->getAmount();
        $negative = false;

        if ('-' === $valueBase[0]) {
            $negative = true;
            $valueBase = substr($valueBase, 1);
        }

        $subunit = (new LumenCurrencies())->subunitFor($money->getCurrency());
        $valueBase = Number::roundMoneyValue($valueBase, $subunit, $subunit);
        $valueLength = strlen($valueBase);

        if ($valueLength > $subunit) {
            $formatted = substr($valueBase, 0, $valueLength - $subunit);

            if ($subunit) {
                $formatted .= '.';
                $formatted .= substr($valueBase, $valueLength - $subunit);
            }
        } else {
            $formatted = '0.'.str_pad('', $subunit - $valueLength, '0').$valueBase;
        }

        $formatted = LumenCurrencies::SYMBOL.$formatted;

        if (true === $negative) {
            $formatted = '-'.$formatted;
        }

        return $formatted;
    }
}