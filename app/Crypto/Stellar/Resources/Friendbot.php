<?php

namespace App\Crypto\Stellar\Resources;


use App\Crypto\Stellar\Response;

class Friendbot extends AbstractResource
{

    /**
     * Returns the service path for interacting with this resource.
     *
     * @return string
     */
    public function path(): string
    {
        return 'friendbot';
    }

    /**
     * Creates a new test account.
     *
     * @param string $publicKey
     *
     * @return \App\Crypto\Stellar\Response
     * @throws \HttpException
     */
    public function createAccount(string $publicKey): Response
    {
        return $this->addQueryArgument('addr', $publicKey)->request('get');
    }
}