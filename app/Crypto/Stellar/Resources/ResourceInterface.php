<?php

namespace App\Crypto\Stellar\Resources;


interface ResourceInterface
{
    /**
     * Returns the service path for interacting with this resource.
     *
     * @return string
     */
    public function path(): string;
}