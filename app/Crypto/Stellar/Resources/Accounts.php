<?php

namespace App\Crypto\Stellar\Resources;


class Accounts extends AbstractResource
{
    /**
     * Returns the service path for interacting with this resource.
     *
     * @return string
     */
    public function path(): string
    {
        return '/accounts';
    }
}