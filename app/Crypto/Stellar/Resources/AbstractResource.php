<?php

namespace App\Crypto\Stellar\Resources;


use App\Crypto\Stellar\InteractsUsingREST;
use App\Crypto\Stellar\Response;
use App\Crypto\Stellar\Server;
use GuzzleHttp\Psr7\Uri;

abstract class AbstractResource implements ResourceInterface
{
    use InteractsUsingREST;

    /** @var Server  */
    protected $server;

    /** @var string|null */
    protected $id;

    /** @var ResourceInterface  */
    protected $parent;

    /**
     * AbstractResource constructor.
     *
     * @param Server      $server
     * @param string|null $id
     */
    public function __construct(Server $server, string $id = null, ResourceInterface $parent = null)
    {
        $this->id = $id;
        $this->parent = $parent;
        $this->server = $server;
    }

    /**
     * @inheritdoc
     */
    public function item(string $id): ResourceInterface
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function collection(): ResourceInterface
    {
        $this->id = null;
        return $this;
    }

    /**
     * Returns the effective URI to use for the resource.
     *
     * @param array $pathParams
     *
     * @return Uri
     */
    public function getRequestUrl(array $pathParams = []): Uri
    {
        $path = static::path();
        # get the path for the resource
        if (!empty($this->id)) {
            # requesting data off a resource item
            $path .= '/' . $this->id;
        }
        $path .= !empty($pathParams) ? '/' . implode('/', $pathParams) : '';
        # append the rest of the path
        if (!empty($path) && $path[0] !== '/') {
            $path = '/' . $path;
        }
        return $this->server->getUrl()->withPath($path);
    }

    /**
     * Sends the HTTP request, using the specified method.
     *
     * @param string $method
     * @param array  $path
     *
     * @return Response
     * @throws \HttpException
     */
    public function request(string $method, array $path = []): Response
    {
        return $this->send($method, $this->server->getHttpClient(), $path);
    }

    /**
     * @return bool
     */
    public function validate(): bool
    {
        return true;
    }
}