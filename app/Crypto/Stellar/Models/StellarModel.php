<?php

namespace App\Crypto\Stellar\Models;


use App\Crypto\Stellar\Currency\LumenCurrencies;
use App\Crypto\Stellar\Currency\LumenMoneyParser;
use Illuminate\Support\Facades\Log;

class StellarModel
{
    /** @var array  */
    protected $attributes = [];

    /**
     * StellarModel constructor.
     *
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->attributes = $attributes;
    }

    /**
     * @param array $attributes
     *
     * @return $this
     */
    public function setAttributes(array $attributes = [])
    {
        $this->attributes = $attributes;
        return $this;
    }

    /**
     * @param $name
     *
     * @return mixed|null
     */
    public function __get($name)
    {
        if (!isset($this->attributes[$name])) {
            return null;
        }
        $record = $this->attributes[$name];
        # get the record
        try {
            $model = null;
            switch ($name) {
                case 'balance':
                    if (!isset($record['balance'])) {
                        # probably a string value for the amount
                        return (new LumenMoneyParser())->parse(LumenCurrencies::SYMBOL.$record);
                    }
                    return new Balance($record);
                    break;
                case 'balances':
                    if (!is_array($record) && !isset($record[0]['balance'])) {
                        throw new \UnexpectedValueException('There was no relevant "balance" key found in the first entry of the array');
                    }
                    return collect($this->attributes['balances'])->map(function ($balance) {
                        return new Balance($balance);
                    });
                    break;
                case 'signer':
                    if (!is_array($record) && !isset($record['public_key'])) {
                        throw new \UnexpectedValueException('There was no relevant "public_key" key found in the array');
                    }
                    return new Signer($record);
                    break;
                case 'signers':
                    if (!is_array($record) && !isset($record[0]['public_key'])) {
                        throw new \UnexpectedValueException('There was no relevant "public_key" key found in the first entry of the array');
                    }
                    return collect($this->attributes['signers'])->map(function ($signer) {
                        return new Signer($signer);
                    });
                    break;
            }
        } catch (\UnexpectedValueException $e) {
            Log::warning($e->getMessage());
        }
        return $record;
    }

    /**
     * @param $name
     * @param $value
     *
     * @return $this
     */
    public function __set($name, $value)
    {
        $this->attributes[$name] = $value;
        return $this;
    }

    /**
     * @return array
     */
    public function toArray()
    {
        return $this->attributes;
    }
}