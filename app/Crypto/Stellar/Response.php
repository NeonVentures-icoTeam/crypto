<?php

namespace App\Crypto\Stellar;


use Psr\Http\Message\RequestInterface;

class Response
{
    /** @var string */
    private $rawResponse;

    /** @var int  */
    private $httpStatus;

    /** @var array */
    private $data = [];

    /** @var RequestInterface  */
    private $request;

    /**
     * Response constructor.
     *
     * @param string                $response
     * @param int                   $httpStatus
     * @param RequestInterface|null $request
     */
    public function __construct(string $response, int $httpStatus = 200, RequestInterface $request = null)
    {
        $this->httpStatus = $httpStatus;
        $this->request = $request;
        $this->rawResponse = $response;
        $jsonData = $this->decodeJson($response);
        $this->data = $jsonData ?: [];
    }

    /**
     * @param string $name
     *
     * @return mixed|null
     */
    public function __get($name)
    {
        return array_key_exists($name, $this->data) ? $this->data[$name] : null;
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function __isset($name)
    {
        return isset($this->data[$name]);
    }

    /**
     * Attempts to decode a JSON string to an array; if it fails, it just returns an array with the data index as the
     * string.
     *
     * @param string $jsonString
     *
     * @return array
     */
    private function decodeJson(string $jsonString)
    {
        $json = json_decode($jsonString, true);
        return $json ?: ['data' => $jsonString];
    }

    /**
     * Returns the raw response that was passed into the constructor.
     *
     * @return string
     */
    public function getRawResponse(): string
    {
        return $this->rawResponse;
    }

    /**
     * Is it a success response?
     *
     * @return bool
     */
    public function isSuccessful(): bool
    {
        return $this->httpStatus >= 200 && $this->httpStatus < 300;
    }

    /**
     * Returns the HTTP status of the request.
     *
     * @return int
     */
    public function getHttpStatus(): int
    {
        return (int) $this->httpStatus;
    }

    /**
     * Decodes XDR data in the response based on the specified key. The _xdr string is appended to the end of the key
     * name.
     * For instance, to get the envelope_xdr value, you call:
     *
     *  Response::getXdrData('envelope');
     *
     * @param string $name
     *
     * @return bool|null|string
     */
    public function getXdrData(string $name = 'envelope')
    {
        $name .= '_xdr';
        if (empty($this->data[$name]) && empty($this->data['extras'][$name])) {
            return null;
        }
        return base64_decode($this->data[$name] ?? $this->data['extras'][$name]);
    }

    /**
     * Returns the value of the "data" key in the response if available, else it returns the parsed response.
     * If a model class is specified, it returns an instance of that class.
     *
     * @param bool        $asObject
     * @param string|null $model
     *
     * @return array|object
     */
    public function getData(bool $asObject = false, string $model = null)
    {
        if (!empty($model) && class_exists($model)) {
            return new $model($this->data);
        }
        return $asObject && $this->data !== null ? (object) $this->data : $this->data;
    }

    /**
     * Returns the errors in the response, if any.
     *
     * @return object
     */
    public function getError()
    {
        if ($this->isSuccessful()) {
            return null;
        }
        return (object) $this->data;
    }

    /**
     * Returns a summary of the request. This will usually be available in the case of a failure.
     *
     * @return array
     */
    public function dumpRequest(): array
    {
        if (empty($this->request)) {
            return ['http_status' => $this->httpStatus, 'response' => $this->data];
        }
        $this->request->getBody()->rewind();
        # rewind the request
        $size = $this->request->getBody()->getSize() ?: 1024;
        # get the size, we need it to be able to read through; else we assume 1024
        $bodyParams = json_decode($this->request->getBody()->read($size), true);
        # the body parameters
        $possibleJsonString = (string) $this->request->getBody();
        # cast it to a string
        $jsonData = json_decode($possibleJsonString);
        $uri = $this->request->getUri();
        $url = $uri->getScheme() . '://' . $uri->getAuthority() . '/' . $uri->getPath();
        return [
            'http_status' => $this->httpStatus,
            'endpoint' => $url,
            'params'   => $bodyParams,
            'response' => $jsonData ?: $possibleJsonString
        ];
    }
}